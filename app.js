console.log("log to console");
console.log(123);
console.log(true);
var greeting = "Hello";
console.log(greeting);
console.log([1, 3, 5, 6]); // log array
console.log({a: 1, b: 2}); // log object
console.table({a: 1, b: 2, c: 10});
console.error("This is some error");

console.time("Hello");
console.log(123);
console.log(123);
console.log(123);
console.log(123);
console.timeEnd("Hello");